FROM openjdk:8-jdk-alpine
COPY target/*.jar /app.jar
COPY *.properties /

RUN mkdir -p /devops \
&& mv /*.jar /devops/app.jar \
&& mv /*.properties /devops/. \
&& chown nobody:nobody -R /devops

USER nobody
WORKDIR /devops
ENTRYPOINT ["java","-jar","-Dspring.config.location=file:///devops/mysql.properties","/devops/app.jar"]
